KINBIOTICS Cloud API
====================

Small API to upload fast5 files to the cloud workflow and start the workflow execution for a project, if the final\_summary.txt is received.

## Setup

This has to run directly on the cloud workflow host, so it can call the nextflow workflow.

First the `apache2`, `apache2-dev` and `pyhton3-dev` packages have to be installed. Afterwads the python modules from `requirements.txt` need to be set up.  

The webserver user needs to be able to run nextflow via sudo, e.g. by adding 

`www-data ALL = (ubuntu) NOPASSWD: /home/ubuntu/nextflow`

to `/etc/sudoers`, if the webserver user is www-data, the workflow user is ubuntu and the nextflow binary is stored as /home/ubuntu/nextflow.

Also variables in `src/config.py` and `src/run_wf.sh` likely need to be adjusted.

Then the API can be run from the `src` directory with
 
`mod_wsgi-express start-server --user=www-data wsgi.py --limit-request-body 10000000000`
