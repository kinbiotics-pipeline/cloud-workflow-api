import glob
import subprocess
import time
import os

from config import Config


def start_workflow(project, summary):
    """
    Wait for remaining fast5 files and start the cloud-workflow.

    :param project: Project name to start the workflow for
    :param summary: Filename of the final_summery file to get project information from
    :return: Null
    """

    # wait for all fast5 files to be uploaded for up to Config.PROCESS_DELAY_TIMEOUT seconds

    # get number of expected fast5 files from the summary
    with open(os.path.join(Config.UPLOAD_FOLDER, project, summary)) as final_summary:

        for _ in final_summary:
            line = final_summary.readline()

            if line.startswith('fast5_files_in_final_dest='):
                number_of_fast5 = int(line.split('=')[1])
                break

    # wait until the timeout for all fast5 files to be there
    timeout = time.time() + Config.PROCESS_DELAY_TIMEOUT
    while time.time() < timeout:
        try:
            fast5s = os.listdir(os.path.join(Config.UPLOAD_FOLDER, project, 'fast5'))
        except FileNotFoundError:
            # maybe just no fast5s yet, but currently uploading
            fast5s = []

        if len(fast5s) >= number_of_fast5:
            break

    print('calling wf wrapper')
    # call a wrapper script which starts the workflow
    subprocess.Popen([Config.WORKFLOW_WRAPPER_SCRIPT, "--path", os.path.join(Config.UPLOAD_FOLDER, project), '--gtdb', Config.GTDB_PATH, '--db_api_server', Config.DATABASE_API_SERVER], start_new_session=True)
