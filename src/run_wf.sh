#!/bin/bash

workflow_dir='/kinbiotics/cloud-workflow/'
workflow_name='main.nf'
outdir_base='output'
nextflow_binary='/home/ubuntu/bin/nextflow'
workflow_user='ubuntu'

while [[ $# -gt 0 ]]; do
  case "$1" in
    --path)
      projectpath="$2"
      shift
      shift
      ;;
    --gtdb)
      gtdbpath="$2"
      shift
      shift
      ;;
    --db_api_server)
      dbapiip="$2"
      shift
      shift
      ;;
    *)
      # we should not get here
      echo "[run_wf.sh] Error: Unknown argument ${1}"
      exit 1;
      ;;
  esac
done

cd "${workflow_dir}" || { echo '[run_wf.sh] Error: could not change to workflow directory'; exit 3 ; }

logger "[run_wf.sh] Running workflow ${workflow_dir}/${workflow_name} with data from ${projectpath} and gtdb ${gtdbpath}."

sudo -u "${workflow_user}"\
 "${nextflow_binary}" run "${workflow_name}" \
 --fast5 "${projectpath}/fast5" \
 --final_summary "${projectpath}/final_summary*" \
 --gtdb "${gtdbpath}" \
 --outdir "${outdir_base}/${projectpath##*/}" \
 --db_api_server "${dbapiip}" \
 -work-dir "${workflow_dir}/work"

