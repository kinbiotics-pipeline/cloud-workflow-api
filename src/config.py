# noinspection PyInterpreter

class Config:
    UPLOAD_FOLDER = '/kinbiotics/api/upload'

    WORKFLOW_WRAPPER_SCRIPT = '/kinbiotics/api/src/run_wf.sh'

    GTDB_PATH = '/kinbiotics/databases/gtdb/release214/'

    DATABASE_API_SERVER = '192.168.0.55:8000'

    PROCESS_DELAY_TIMEOUT = 40  # in s, has to be smaller than webserver timeout

    INVALID_SCHEMA_CHARS = '[^A-Z,a-z,0-9]+'
    SCHEMA_REPLACEMENT_CHAR = '_'
