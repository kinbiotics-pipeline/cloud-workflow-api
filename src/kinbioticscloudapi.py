import os
import re

from flask import Flask, request, send_from_directory
from werkzeug.utils import secure_filename

from config import Config
from workflow import start_workflow

app = Flask(__name__)


def sanitize_schema_name(name):

    r = re.compile(Config.INVALID_SCHEMA_CHARS)

    subs_name = re.sub(r, Config.SCHEMA_REPLACEMENT_CHAR, name)

    if subs_name.startswith((Config.SCHEMA_REPLACEMENT_CHAR, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9')):
        return f"X{subs_name}"
    else:
        return subs_name


@app.get('/<project>/fast5/<fast5>')
def get_fast5_file(project, fast5):
    """
    Fetch a specific project fast5 file.

    :param project: Name of the project
    :param fast5: File to fetch
    :return: Specified file
    """

    project = sanitize_schema_name(project)
    return send_from_directory(Config.UPLOAD_FOLDER, os.path.join(project, 'fast5', fast5))


@app.put('/<project>/fast5/<fast5>')
def store_fast5(project, fast5):
    """
    Upload a new fast5 file to the project.

    :param project: Name of the project
    :param fast5: The fast5 file to upload
    :return: nothing / HTTP status 201
    """

    filename = secure_filename(fast5)
    project = sanitize_schema_name(secure_filename(project))

    savepath = os.path.join(Config.UPLOAD_FOLDER, project, 'fast5')
    if not os.path.exists(savepath):
        os.makedirs(savepath)

    with open(os.path.join(savepath, filename), 'wb') as f:
        f.write(request.stream.read())

    return '', 201


@app.put('/<project>/<file>')
def store_data(project, file):
    """
    Upload a new file to the project.
    If the file name starts with 'final_summary' the cloud workflow is executed.

    :param project: Name of the project
    :param file: The file to upload
    :return: nothing / HTTP status 201
    """

    filename = secure_filename(file)
    project = sanitize_schema_name(secure_filename(project))

    savepath = os.path.join(Config.UPLOAD_FOLDER, project)
    if not os.path.exists(savepath):
        os.makedirs(savepath)

    with open(os.path.join(savepath, filename), 'wb') as f:
        f.write(request.stream.read())

    if filename.lower().startswith('final_summary'):
        start_workflow(project, filename)

    return '', 200


if __name__ == '__main__':
    app.run()
